#!/usr/bin/env bash

# Ideas from
#  - https://github.com/webpro/dotfiles/blob/master/install.sh 

# Get current dir (so run this script from anywhere)

export DOTFILES_DIR EXTRA_DIR
DOTFILES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
EXTRA_DIR="$HOME/.extra"

# Update dotfiles itself first

[ -d "$DOTFILES_DIR/.git" ] && git --work-tree="$DOTFILES_DIR" --git-dir="$DOTFILES_DIR/.git" pull origin master






# # Install base pacakage
# sudo apt-get install vim
# sudo apt-get install mutt urlscan
# sudo apt-get install git
# sudo apt-get install cmus
# 



# Bunch of symlinks for Shell

ln -sfv "$DOTFILES_DIR/shell/.bashrc" ~
ln -sfv "$DOTFILES_DIR/shell/.sh_aliases" ~
ln -sfv "$DOTFILES_DIR/shell/.zshrc" ~
ln -sfv "$DOTFILES_DIR/shell/.zshrc.local" ~
ln -sfv "$DOTFILES_DIR/shell/.zshrc.zni" ~
ln -sfv "$DOTFILES_DIR/git/.gitconfig" ~
ln -sfv "$DOTFILES_DIR/profile/.newsbeuter_urls" ~/.newsbeuter/urls
ln -sfv "$DOTFILES_DIR/profile/Rprofile" ~/.Rprofile
ln -sfv "$DOTFILES_DIR/profile/tmux.conf" ~/.tmux.conf
ln -sfv "$DOTFILES_DIR/vim/" ~/.vim/
mkdir $HOME/.vim_temp/swap/
mkdir $HOME/.vim_temp/backup/
mkdir $HOME/.vim_temp/undo/
ln -sfv "$DOTFILES_DIR/openbox/lxde-rc.xml" ~/.config/openbox/
