Personal dotfiles for mutt
--------------------------

Credits: important ideas come from [fmenable](https://github.com/fmenabe/mutt)
mutt repository

For each mailbox I created a dedicated configuration file, to be launched from
the CLI:

    mutt -F mailboxconfig

Global configurations for mutt are in the file *muttrc*, which is sourced in
each mailconfig. Sourced in *muttrc* are:
  - *muttcolor*
  - *urlscan*: follows url, to be called with CTRL-d when reading a mail  
  - *mailcap*: deals with the rendering of (evil) html mail via *w3m*

**Required dependencies:**  *urlscan* and *w3m* and *mutt-patched* (sidebar)

Happy mutt!
